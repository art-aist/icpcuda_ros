cmake_minimum_required(VERSION 3.8)
project(icpcuda_ros)

enable_language(CXX)
enable_language(CUDA)

set(CMAKE_CXX_STANDARD		17)
set(CMAKE_CXX_STANDARD_REQUIRED	ON)
set(CMAKE_CXX_EXTENSIONS	OFF)
set(CMAKE_CXX_FLAGS_RELEASE	"-Wall -O3 -DNDEBUG")
set(CMAKE_CXX_FLAGS_DEBUG	"-g")
set(CUDA_PROPAGATE_HOST_FLAGS	ON)
set(CUDA_NVCC_FLAGS		"-std=c++17;--extended-lambda;--disable-warnings")

execute_process(COMMAND /usr/local/bin/check_cuda
		RESULT_VARIABLE CUDA_RETURN_CODE
		OUTPUT_VARIABLE ARCH)
message(STATUS "CUDA Architecture: -arch=sm_${ARCH}")
#set(CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS};--generate-code arch=compute_${ARCH},code=\"compute_${ARCH},sm_${ARCH}\")

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "/usr/local/share/cmake/TU")
include(BuildType)

find_package(CUDA REQUIRED)
find_package(OpenCV REQUIRED)

include_directories(${CUDA_INCLUDE_DIRS})
include_directories(${EIGEN_INCLUDE_DIRS})

find_package(CUDA 11 REQUIRED)
find_package(TULibs REQUIRED)
find_package(catkin REQUIRED
  COMPONENTS
    roscpp
    roslib
    image_transport
    sensor_msgs
    ddynamic_reconfigure
    pcl_ros
    pcl_conversions
    aist_utility
    cu_bridge
)

catkin_package(
  CATKIN_DEPENDS
    roscpp
    image_transport
    sensor_msgs
)

###########
## Build ##
###########
include_directories(
  ${catkin_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIRS}
  ${TULIBS_INCLUDE_DIRS}
  ${CUDA_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
)

cuda_add_executable(${PROJECT_NAME}
  src/main.cu
  src/CloudRegistrator.cu
  src/ICPOdometry.cu
  src/containers/initialization.cpp
  src/containers/device_memory.cpp
)

add_dependencies(${PROJECT_NAME}
  ${${PREOJECT_NAME}_EXPORTED_TARGETS}
  ${catkin_EXPORTED_TARGETS}
)

target_link_libraries(${PROJECT_NAME}
  ${catkin_LIBRARIES}
  ${roscpp_LIBRARIES}
  ${Boost_LIBRARIES}
  ${CUDA_LIBRARIES}
  ${TUCUDA_LIB}
  ${OpenCV_LIBRARIES}
)

#############
## Install ##
#############
install(
  TARGETS
    ${PROJECT_NAME}
  ARCHIVE
    DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY
    DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME
    DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
