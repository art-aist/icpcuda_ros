/*!
* \file		main.cpp
* \author	Toshio UESHIBA
*/
#include <ros/ros.h>
#include "CloudRegistrator.h"

int
main(int argc, char** argv)
{
    ros::init(argc, argv, "icpcuda_ros");
    ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME,
				   ros::console::levels::Debug);

    try
    {
	ros::NodeHandle			nh("~");
	icpcuda_ros::CloudRegistrator	cloud_registrator(nh);
	cloud_registrator.run();
    }
    catch (const std::exception& err)
    {
	std::cerr << err.what() << std::endl;
	return 1;
    }

    return 0;
}
