#include "ICPOdometry.h"

#include <chrono>
#include <fstream>
#include <iomanip>
#include <pangolin/image/image_io.h>
#include <Eigen/Eigen>

std::ifstream asFile;
std::string directory;

void
tokenize(const std::string &str, std::vector<std::string> &tokens,
	 std::string delimiters = " ")
{
    tokens.clear();

    std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    std::string::size_type pos = str.find_first_of(delimiters, lastPos);

    while (std::string::npos != pos || std::string::npos != lastPos)
    {
	tokens.push_back(str.substr(lastPos, pos - lastPos));
	lastPos = str.find_first_not_of(delimiters, pos);
	pos = str.find_first_of(delimiters, lastPos);
    }
}

uint64_t
loadDepth(pangolin::Image<uint16_t> &depth)
{
    std::string currentLine;
    std::vector<std::string> tokens;
    std::vector<std::string> timeTokens;

    do
    {
	getline(asFile, currentLine);
	tokenize(currentLine, tokens);
    } while (tokens.size() > 2);

    if (tokens.size() == 0)
	return 0;

    std::string depthLoc = directory;
    depthLoc.append(tokens[1]);

    pangolin::TypedImage depthRaw =
	pangolin::LoadImage(depthLoc, pangolin::ImageFileTypePng);

    pangolin::Image<uint16_t> depthRaw16(
	(uint16_t *)depthRaw.ptr, depthRaw.w, depthRaw.h,
	depthRaw.w * sizeof(uint16_t));

    tokenize(tokens[0], timeTokens, ".");

    std::string timeString = timeTokens[0];
    timeString.append(timeTokens[1]);

    uint64_t time;
    std::istringstream(timeString) >> time;

    for (unsigned int i = 0; i < 480; i++)
    {
	for (unsigned int j = 0; j < 640; j++)
	{
	    depth.RowPtr(i)[j] = depthRaw16(j, i) / 5;
	}
    }

    depthRaw.Dealloc();

    return time;
}

void
outputFreiburg(const std::string& filename, uint64_t timestamp,
	       const TU::cu::Rigidity<float, 3>& currentPose)
{
    std::ofstream	fout;
    fout.open(filename.c_str(), std::fstream::app);

    const auto&			t = currentPose.t();
    const Eigen::Vector3f	trans(t.x, t.y, t.z);
    fout << std::setprecision(6) << std::fixed
	 << double(timestamp)/1000000.0 << ' '
	 << trans(0) << ' ' << trans(1) << ' ' << trans(2) << ' ';

    const auto&		R = currentPose.R();
    Eigen::Matrix3f	rot;
    rot << R.x.x, R.x.y, R.x.z,
	   R.y.x, R.y.y, R.y.z,
	   R.z.x, R.z.y, R.z.z;
    Eigen::Quaternionf	currentCameraRotation(rot);
    fout << currentCameraRotation.x() << ' '
	 << currentCameraRotation.y() << ' '
	 << currentCameraRotation.z() << ' '
	 << currentCameraRotation.w() << std::endl;
}

uint64_t
getCurrTime()
{
    return std::chrono::duration_cast<std::chrono::microseconds>(
	std::chrono::high_resolution_clock::now().time_since_epoch())
	.count();
}

int
main(int argc, char *argv[])
{
    using registrator_type	= TU::cu::ICPOdometry;
    using transform_type	= registrator_type::transform_type;
    
    assert((argc == 2 || argc == 3) &&
	   "Please supply the depth.txt dir as the first argument");

    directory.append(argv[1]);

    if (directory.at(directory.size() - 1) != '/')
    {
	directory.append("/");
    }

    std::string associationFile = directory;
    associationFile.append("depth.txt");
    asFile.open(associationFile.c_str());
    assert(!asFile.eof() && asFile.is_open());

    std::ofstream	file;
    file.open("output.poses", std::fstream::out);
    file.close();

    cudaDeviceProp	prop;
    cudaGetDeviceProperties(&prop, 0);
    std::string	dev(prop.name);
    std::cout << dev << std::endl;

    pangolin::ManagedImage<uint16_t>	data(640, 480);
    pangolin::Image<uint16_t>		depth(data.w, data.h, data.pitch,
					      (uint16_t *)data.ptr);
    float		mean = 0.0f;
    int			count = 0;
    transform_type	T_wc;
    T_wc.initialize();
    
    registrator_type	icpOdom;
    float		K[]{528.0, 0, 319.5, 0, 528.0, 239.5, 0.0, 0.0, 1.0};
    float		D[]{0.0, 0.0, 0.0, 0.0};
    
    while (!asFile.eof())
    {
	uint64_t	timestamp = loadDepth(depth);
	icpOdom.input_frame(depth.ptr, depth.w, depth.h, K, D);

	uint64_t	tick = getCurrTime();
	const auto	T_pc = icpOdom.get_incremental_transform();
	T_wc = T_wc * T_pc;
	uint64_t	tock = getCurrTime();

	mean = (float(count)*mean + (tock - tick)/1000.0f)/float(count + 1);
	count++;

	std::cout << std::setprecision(4) << std::fixed << "\rICP: " << mean
		  << "ms";
	std::cout.flush();

	outputFreiburg("output.poses", timestamp, T_wc);
    }

    std::cout << std::endl;

    std::cout << "ICP speed: " << int(1000.f / mean) << "Hz" << std::endl;

    return 0;
}
