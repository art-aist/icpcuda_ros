// Software License Agreement (BSD License)
//
// Copyright (c) 2021, National Institute of Advanced Industrial Science and Technology (AIST)
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  * Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//  * Neither the name of National Institute of Advanced Industrial
//    Science and Technology (AIST) nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Author: Toshio Ueshiba
//
/*!
  \file		CloudRegistrator.cpp
  \author	Toshio Ueshiba
*/
#include <boost/algorithm/string.hpp>
#include <boost/interprocess/sync/file_lock.hpp>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <pcl_ros/point_cloud.h>
#include <tf/tf.h>
#include "CloudRegistrator.h"

namespace icpcuda_ros
{
/************************************************************************
*  global functions							*
************************************************************************/
void
transform_pointcloud(sensor_msgs::PointCloud2& cloud,
		     const TU::cu::Rigidity<float, 3>& transform)
{
    if (cloud.width == 0 || cloud.height == 0)
	return;

    sensor_msgs::PointCloud2Iterator<float>	xyz(cloud, "x");
    for (uint32_t v = 0; v < cloud.height; ++v)
    {
	for (uint32_t u = 0; u < cloud.width; ++u)
	{
	    const TU::cu::vec<float, 3>	p{xyz[0], xyz[1], xyz[2]};
	    const auto			q = transform(p);
	    xyz[0] = q.x;
	    xyz[1] = q.y;
	    xyz[2] = q.z;
	    ++xyz;
	}
    }
}

/************************************************************************
*  class CloudRegistrator						*
************************************************************************/
CloudRegistrator::CloudRegistrator(const ros::NodeHandle& nh)
    :_nh(nh),
     _it(_nh),
     _camera_sub(_it.subscribeCamera("/depth", 1,
				     &CloudRegistrator::camera_cb, this)),
     _cloud_pub(_nh.advertise<cloud_t>("pointcloud", 1)),
     _ddr(_nh),
     _odometry(), _out("/tmp/output.poses"),
     _clouds(), _current_cloud(_clouds.begin(), _clouds.end())
{
    for (auto&& cloud : _clouds)
    {
	cloud.width = 0;
	cloud.height = 0;
    }

    _T_wc.initialize();

  // Setup ddynamic_reconfigure server.
    _ddr.registerVariable<double>(
	"dist_thresh", _odometry.get_dist_thresh(),
	boost::bind(&CloudRegistrator::set_registrator_param<double, float>,
		    this, &registrator_type::set_dist_thresh, _1,
		    "dist_thresh"),
	"Allowed maximum distance between corresponding points", 0.001, 0.2);
    _ddr.registerVariable<double>(
	"angle_thresh", _odometry.get_angle_thresh(),
	boost::bind(&CloudRegistrator::set_registrator_param<double, float>,
		    this, &registrator_type::set_angle_thresh, _1,
		    "angle_thresh"),
	"Allowed maximum angle between normals at corresponding points",
	5.0, 45.0);
    _ddr.publishServicesTopics();
}

void
CloudRegistrator::run()
{
    ros::spin();
}

/*
 *  private member functions
 */
template <class T, class U> void
CloudRegistrator::set_registrator_param(U (registrator_type::* setter)(U),
					T value, const std::string& name)
{
    value = (_odometry.*setter)(value);
    ROS_INFO_STREAM("(CloudRegistrator) set " << name
		    << " to " << std::boolalpha << value);
}

void
CloudRegistrator::camera_cb(const image_cp& depth,
			    const camera_info_cp& camera_info)
{
    try
    {
	using namespace	sensor_msgs;

      // Set data
	if (depth->encoding == image_encodings::TYPE_32FC1)
	    _odometry.input_frame(reinterpret_cast<const float*>(
				      depth->data.data()),
				  camera_info->width, camera_info->height,
				  std::begin(camera_info->K),
				  std::begin(camera_info->D));
	else
	    _odometry.input_frame(reinterpret_cast<const uint16_t*>(
				      depth->data.data()),
				  camera_info->width, camera_info->height,
				  std::begin(camera_info->K),
				  std::begin(camera_info->D));

      // Compute motion from current to previous frame.
	const auto	T_pc = _odometry.get_incremental_transform();

      // Compute camera to world transform.
	_T_wc = _T_wc * T_pc;

	outputFreiburg(camera_info->header.stamp.toSec());

      // Store new cloud in ring buffer.
	*_current_cloud = aist_utility::create_pointcloud<float>(*camera_info,
								 *depth, true);
	aist_utility::add_rgb_to_pointcloud(*_current_cloud,
					    _cmap[_current_cloud.position()]);
	transform_pointcloud(*_current_cloud, _T_wc);

      // Merge former clouds into current cloud.
	_registered_cloud = *_current_cloud;
	for (auto cloud = _current_cloud; ++cloud != _current_cloud; )
	{
	    const auto	tmp = _registered_cloud;
	    pcl::concatenatePointCloud(*cloud, tmp, _registered_cloud);
	}
	++_current_cloud;

      // Publish merged cloud.
	_cloud_pub.publish(_registered_cloud);
    }
    catch (const std::exception& err)
    {
	ROS_ERROR_STREAM(err.what());
    }
}
void
CloudRegistrator::outputFreiburg(double timestamp)
{
    const auto&		t = _T_wc.t();
    const auto&		R = _T_wc.R();
    Eigen::Matrix3f	rot;
    rot << R.x.x, R.x.y, R.x.z,
	   R.y.x, R.y.y, R.y.z,
	   R.z.x, R.z.y, R.z.z;
    Eigen::Quaternionf	q(rot);

    _out << std::setprecision(6) << std::fixed << timestamp << ' '
	 << t.x << ' ' << t.y << ' ' << t.z << ' '
	 << q.x() << ' ' << q.y() << ' ' << q.z() << ' ' << q.w()
	 << std::endl;
}

}	// namespace cuda_cloud_registrator
