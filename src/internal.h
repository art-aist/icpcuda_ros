/*
 * Software License Agreement (BSD License)
 *
 *  Point Cloud Library (PCL) - www.pointclouds.org
 *  Copyright (c) 2011, Willow Garage, Inc.
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once

#include "containers/device_array.hpp"
#include "containers/safe_call.hpp"

#include <cuda_runtime_api.h>
#include <vector_types.h>
#include <TU/cu/vec.h>

/************************************************************************
*  static functions							*
************************************************************************/
static inline int
divUp(int total, int grain)
{
    return (total + grain - 1) / grain;
}

#if defined(__NVCC__)
/************************************************************************
*  compute_points()							*
************************************************************************/
namespace device
{
  template <class T> __global__ void
  pyramid_down(const PtrStepSz<T> src, PtrStepSz<T> dst, float sigma_color)
  {
      const int		x = blockIdx.x * blockDim.x + threadIdx.x;
      const int		y = blockIdx.y * blockDim.y + threadIdx.y;

      if (x >= dst.cols || y >= dst.rows)
	  return;

      constexpr int	D = 5;

      const auto	center = src.ptr(2*y)[2*x];
      
      const int		x_mi = max(0, 2*x - D/2) - 2*x;
      const int		y_mi = max(0, 2*y - D/2) - 2*y;

      const int		x_ma = min(src.cols, 2*x - D/2 + D) - 2*x;
      const int		y_ma = min(src.rows, 2*y - D/2 + D) - 2*y;

      constexpr float	weights[] = {0.375f, 0.25f, 0.0625f};

      float		sum  = 0;
      float		wall = 0;

      for (int yi = y_mi; yi < y_ma; ++yi)
	  for (int xi = x_mi; xi < x_ma; ++xi)
	  {
	      const auto	val = src.ptr(2*y + yi)[2*x + xi];

	      if (abs(val - center) < 3*sigma_color)
	      {
		  sum  += val * weights[abs(xi)] * weights[abs(yi)];
		  wall += weights[abs(xi)] * weights[abs(yi)];
	      }
	  }
      
      dst.ptr(y)[x] = static_cast<T>(sum/wall);
  }
}	// namespace device

template <class T> void
pyramid_down(const DeviceArray2D<T> &src, DeviceArray2D<T> &dst)
{
    dst.create(src.rows()/2, src.cols()/2);

    
    const dim3	block(32, 8);
    const dim3	grid(divUp(dst.cols(), block.x), divUp(dst.rows(), block.y));

    const float	sigma_color = 30;

    device::pyramid_down<T><<<grid, block>>>(src, dst, sigma_color);
    cudaSafeCall(cudaGetLastError());
};

/************************************************************************
*  compute_points()							*
************************************************************************/
namespace device
{
  template <class T> __device__ __forceinline__
  T	to_meters(T val)		{ return val; }
  template <class T> __device__ __forceinline__
  T	to_meters(unsigned short val)	{ return T(val)*T(0.001); }

  template <class S, class T> __global__ void
  create_points(TU::cu::Intrinsics<S> intr, const PtrStepSz<T> depth,
		PtrStep<TU::cu::vec<S, 3> > points, S depthCutoff)
  {
      const int	u = threadIdx.x + blockIdx.x * blockDim.x;
      const int	v = threadIdx.y + blockIdx.y * blockDim.y;

      if (u < depth.cols && v < depth.rows)
      {
	// load and convert: mm -> meters
	  const auto	d = to_meters<S>(depth.ptr(v)[u]);

	  if (0 < d && d < depthCutoff)
	      points.ptr(v)[u] = intr(u, v, d);
	  else
	      points.ptr(v)[u] = {__int_as_float(0x7fffffff),
				  __int_as_float(0x7fffffff),
				  __int_as_float(0x7fffffff)};
      }
  }
}	// namespace device

template <class S, class T> void
create_points(const TU::cu::Intrinsics<S>& intr, const DeviceArray2D<T>& depth,
	      DeviceArray2D<TU::cu::vec<S, 3> >& points, S depthCutoff)
{
    points.create(depth.rows(), depth.cols());

    const dim3	block(32, 8);
    const dim3	grid(divUp(depth.cols(), block.x),
		     divUp(depth.rows(), block.y));

    device::create_points<S, T><<<grid, block>>>(intr, depth,
						 points, depthCutoff);
    cudaSafeCall(cudaGetLastError());
}

/************************************************************************
*  create_normals()							*
************************************************************************/
namespace device
{
  template <class T> __global__ void
  create_normals(int rows, int cols,
		 const PtrStep<T> points, PtrStep<T> normals)
  {
      const int	u = threadIdx.x + blockIdx.x * blockDim.x;
      const int	v = threadIdx.y + blockIdx.y * blockDim.y;

      if (u >= cols || v >= rows)
	  return;

      if (u != cols - 1 && v != rows - 1)
      {
	  const auto	p00 = points.ptr(v)[u];
	  const auto	p01 = points.ptr(v)[u + 1];
	  const auto	p10 = points.ptr(v + 1)[u];

	  if (!isnan(p00.x) && !isnan(p01.x) && !isnan(p10.x))
	  {
	      normals.ptr(v)[u] = normalized(cross(p01 - p00, p10 - p00));
	      return;
	  }
      }

      normals.ptr(v)[u] = {__int_as_float(0x7fffffff),
			   __int_as_float(0x7fffffff),
			   __int_as_float(0x7fffffff)};
  }
}	// namespace device

template <class T> void
create_normals(const DeviceArray2D<T>& points, DeviceArray2D<T>& normals)
{
    normals.create(points.rows(), points.cols());

    const dim3	block(32, 8);
    const dim3	grid(divUp(points.cols(), block.x),
		     divUp(points.rows(), block.y));

    device::create_normals<T><<<grid, block>>>(points.rows(), points.cols(),
					       points, normals);
    cudaSafeCall(cudaGetLastError());
}
#endif	// __NVCC__
